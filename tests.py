from collections import OrderedDict
from nose.tools import assert_raises
from mock import patch
import requests
from wirecard.qmore import QMore, QMoreError

def test_qmore_verify_reponse():
    client = QMore('D200001', 'B8AKTPWBRMNBV455FG6M2DANE99WU2', shopId='qmore')

    data = {
        'paymentState': 'SUCCESS',
        'language': 'da',
        'authenticated': 'Yes',
        'purchase_id': '23',
        'responseFingerprintOrder': 'amount,currency,paymentType,financialInstitution,language,orderNumber,paymentState,purchase_id,payment_type,authenticated,anonymousPan,expiry,cardholder,maskedPan,gatewayReferenceNumber,gatewayContractNumber,avsResponseCode,avsResponseMessage,secret,responseFingerprintOrder',
        'cardholder': 'Niels',
        'payment_type': 'ticketpurchase',
        'amount': '40.00',
        'anonymousPan': '0003',
        'expiry': '03/2014',
        'paymentType': 'CCARD',
        'avsResponseMessage': 'Demo AVS ResultMessage',
        'maskedPan': '940000******0003',
        'orderNumber': '14150147',
        'gatewayReferenceNumber': 'DGW_14150147_RN',
        'currency': 'EUR',
        'gatewayContractNumber': 'DemoContractNumber123',
        'financialInstitution': 'Visa',
        'avsResponseCode': 'X',
        'responseFingerprint': '8f7e96cf5fac73ae3bae8290f91ff567ebfadefa7430faefa80b40320252d4f9b30a6f4e39dec33db91f73a3dc6db01fcbc7c7dd813b864e06639e0cd5be3807',
    }
    assert client.verify_response(data)

def test_qmore_verify_response_missing_field():
    client = QMore('D200001', 'B8AKTPWBRMNBV455FG6M2DANE99WU2', shopId='qmore')

    data = {
        'paymentState': 'SUCCESS',
        'authenticated': 'Yes',
        'purchase_id': '23',
        'responseFingerprintOrder': 'amount,currency,paymentType,financialInstitution,language,orderNumber,paymentState,purchase_id,payment_type,authenticated,anonymousPan,expiry,cardholder,maskedPan,gatewayReferenceNumber,gatewayContractNumber,avsResponseCode,avsResponseMessage,secret,responseFingerprintOrder',
        'cardholder': 'Niels',
        'payment_type': 'ticketpurchase',
        'amount': '40.00',
        'anonymousPan': '0003',
        'expiry': '03/2014',
        'paymentType': 'CCARD',
        'avsResponseMessage': 'Demo AVS ResultMessage',
        'maskedPan': '940000******0003',
        'orderNumber': '14150147',
        'gatewayReferenceNumber': 'DGW_14150147_RN',
        'currency': 'EUR',
        'gatewayContractNumber': 'DemoContractNumber123',
        'financialInstitution': 'Visa',
        'avsResponseCode': 'X',
        'responseFingerprint': '8f7e96cf5fac73ae3bae8290f91ff567ebfadefa7430faefa80b40320252d4f9b30a6f4e39dec33db91f73a3dc6db01fcbc7c7dd813b864e06639e0cd5be3807',
    }
    assert not client.verify_response(data)

def test_qmore_verify_response_invalid_data():
    """
    Test verify response invalid data

    """
    client = QMore('D200001', 'B8AKTPWBRMNBV455FG6M2DANE99WU2', shopId='qmore')

    data = {
        'paymentState': 'SUCCESS',
        'language': 'da',
        'authenticated': 'Yes',
        'purchase_id': '23',
        'cardholder': 'Niels',
        'payment_type': 'ticketpurchase',
        'amount': '40.00',
        'anonymousPan': '0003',
        'expiry': '03/2014',
        'paymentType': 'CCARD',
        'avsResponseMessage': 'Demo AVS ResultMessage',
        'maskedPan': '940000******0003',
        'orderNumber': '14150147',
        'gatewayReferenceNumber': 'DGW_14150147_RN',
        'currency': 'EUR',
        'gatewayContractNumber': 'DemoContractNumber123',
        'financialInstitution': 'Visa',
        'avsResponseCode': 'X',
    }
    assert not client.verify_response(data)


def test_qmore_init_datastorage():
    client = QMore('D200001', 'B8AKTPWBRMNBV455FG6M2DANE99WU2', shopId='qmore')
    order_ident = '12345'

    # build the mock response object
    mock_data_storage_init_result = '''storageId=ce161cce3ff466b616d21b05d56981be&storeId=ce161cce3ff466b616d21b05d56981be&javascriptUrl=https%3A%2F%2Fsecure.wirecard-cee.com%2Fqmore%2FdataStorage%2Fjs%2FD200001%2Fqmore%2Fce161cce3ff466b616d21b05d56981be%2FdataStorage.js'''
    mock_response = type('TestResponse', (object,), dict(text=mock_data_storage_init_result))

    request_url = 'https://secure.wirecard-cee.com/qmore/dataStorage/init'
    request_data = OrderedDict([
        ('customerId', 'D200001'),
        ('shopId', 'qmore'),
        ('orderIdent', '12345'),
        ('returnUrl', 'http://www.example.com/return'),
        ('language', 'en'),
        ('requestFingerprint', '3b4e255d631c05eb9382a113ea2475569e6b5c0f014721a60f2c19277ee8834bda3173c7533424a47b2a1d6820700d1ddb68dc751303dbab1b565a04928e47ba'),
    ])
    expected_result = {
        'javascriptUrl': 'https://secure.wirecard-cee.com/qmore/dataStorage/js/D200001/qmore/ce161cce3ff466b616d21b05d56981be/dataStorage.js',
        'storeId': 'ce161cce3ff466b616d21b05d56981be',
        'storageId': 'ce161cce3ff466b616d21b05d56981be'
    }

    with patch.object(requests, 'post', return_value=mock_response) as mock_method:
        data_storage_result = client.init_datastorage(order_ident)
    mock_method.assert_called_once_with(request_url, request_data)
    assert data_storage_result == expected_result

def test_qmore_init_frontend():
    client = QMore('D200001', 'B8AKTPWBRMNBV455FG6M2DANE99WU2', shopId='qmore',)

    # make sure we raise QMoreError when an error occurs
    mock_init_frontend_result = 'error.1.message=PAYMENTTYPE+is+invalid.&error.1.consumerMessage=PAYMENTTYPE+ist+ung%26%23252%3Bltig.&error.1.errorCode=18051&errors=1'
    mock_response = type('TestResponse', (object,), dict(text=mock_init_frontend_result))
 
    with patch.object(requests, 'post', return_value=mock_response) as mock_method:
        with assert_raises(QMoreError):
            result = client.init_frontend('100.00', 'EUR', 'INVALIDPAYMENTTYPE', 'de', 'test order',
                'http://www.example.com/success', 'http://www.example.com/cancel', 'http://www.example.com/failure',
                    'http://www.example.com/service', 'http://www.example.com/confirm', 'chrome', '127.0.0.1')

    # should work fine
    mock_init_frontend_result = 'redirectUrl=https%3A%2F%2Fsecure.wirecard-cee.com%2Fqmore%2Ffrontend%2FD200001qmore%2Fselect.php%3FSID%3D8u8ev2jrc8cs0n94cppphv2036'
    mock_response = type('TestResponse', (object,), dict(text=mock_init_frontend_result))

    request_url = 'https://secure.wirecard-cee.com/qmore/frontend/init'
    request_data = OrderedDict([
        ('customerId', 'D200001'),
        ('shopId', 'qmore'),
        ('amount', '100.00'),
        ('currency', 'EUR'),
        ('paymentType', 'CCARD'),
        ('language', 'de'),
        ('orderDescription', 'test order'),
        ('successUrl', 'http://www.example.com/success'),
        ('cancelUrl', 'http://www.example.com/cancel'),
        ('failureUrl', 'http://www.example.com/failure'),
        ('serviceUrl', 'http://www.example.com/service'),
        ('confirmUrl', 'http://www.example.com/confirm'),
        ('requestFingerprintOrder', 'customerId,shopId,amount,currency,paymentType,language,orderDescription,successUrl,cancelUrl,failureUrl,serviceUrl,confirmUrl,requestFingerprintOrder,consumerUserAgent,consumerIpAddress,myextradata,secret'),
        ('consumerUserAgent', 'chrome'),
        ('consumerIpAddress', '127.0.0.1'),
        ('myextradata', '1'),
        ('requestFingerprint', '8929503fa089aced88c8b9688ead3365b85d1a8e964aa31fa0f83f5ae2c79d2deb5b7f4e66d6dcf47d4aff843ffea38ee6af1d59b3b1764c22293c4634072e9b'),
    ])

    expected_result = 'https://secure.wirecard-cee.com/qmore/frontend/D200001qmore/select.php?SID=8u8ev2jrc8cs0n94cppphv2036'

    with patch.object(requests, 'post', return_value=mock_response) as mock_method:
        result = client.init_frontend('100.00', 'EUR', 'CCARD', 'de', 'test order',
            'http://www.example.com/success', 'http://www.example.com/cancel', 'http://www.example.com/failure',
                'http://www.example.com/service', 'http://www.example.com/confirm', 'chrome', '127.0.0.1', myextradata='1')
    mock_method.assert_called_once_with(request_url, request_data)
    assert result == expected_result

def test_qmore_recur_payment():
    """
    Test recurring payments

    """
    client = QMore('D200001', 'B8AKTPWBRMNBV455FG6M2DANE99WU2', shopId='qmore', password='jcv45z')

    mock_init_frontend_result = 'orderNumber=1234'
    mock_response = type('TestResponse', (object,), dict(text=mock_init_frontend_result))

    request_url = 'https://secure.wirecard-cee.com/qmore/backend/recurPayment'

    request_data = OrderedDict([
        ('customerId', 'D200001'),
        ('shopId', 'qmore'),
        ('password', 'jcv45z'),
        ('language', 'de'),
        ('orderNumber', '1234'),
        ('sourceOrderNumber', '123456'),
        ('autoDeposit', 'NO'),
        ('orderDescription', 'my orderdescription'),
        ('amount', '100.00'),
        ('currency', 'EUR'),
        ('requestFingerprint', 'd89ee82de84ec0fc02364b3f52cbd8cb5aa4854a2a899721e8caf6212685d9689b737a1a110cac6cc6c9d09081c57b736bad18994764b966e53703dd1a3753bc'),
    ])

    with patch.object(requests, 'post', return_value=mock_response) as mock_method:
        result = client.recurring_payment('123456', '100.00', 'my orderdescription', 'de', '1234', autoDeposit='NO')
    mock_method.assert_called_once_with(request_url, request_data)
    assert result == {'orderNumber': '1234'}

